import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { } from '@reduxjs/toolkit'
import { useSelector } from 'react-redux';
import NavBar from './Components/NavBar';
import AvailableSizes from './Components/AvailableSize';
import DisplayProduct from './Components/DisplayProduct';

function App() {
  const { length } = useSelector(store => store.allProduct)
  return (
    <>
      <NavBar />
      <div className="App d-flex"  >
        <div className='w-25 p-5' >
          <AvailableSizes />
        </div >
        <div className='w-75'>
          <p>{length} Product(s) found</p>
          <div className=' d-flex flex-wrap' >
            <DisplayProduct />
          </div>
        </div>
      </div>
    </>
  )
}

export default App
