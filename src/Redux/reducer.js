import { createSlice } from '@reduxjs/toolkit'
import { } from 'react-redux'
import data from '../Components/Data/shopping-cart/data.json'

const allSize = {}
if (Array.isArray(data.products)) {
    data.products.map((product) => {
        if(Array.isArray(product.availableSizes)){
            const productSize = product.availableSizes
            productSize.map((size) => {
                allSize[size] = false
            })
        }
    })
}

const initialState = {
    products: data.products,
    allSizes: allSize,
    selectedSize: [],
    totalItems: 0,
    totalAmount: 0,
    quantity: {},
    cartItems: [],
    installment: 0,
    length: data.products.length,
    display: "hide"
}
const productSlice = createSlice({
    name: "allProduct",
    initialState,
    reducers: {
        show: (state, { payload }) => {
            state.display = "show"
            if (state.quantity.hasOwnProperty(payload.id)) {
                state.quantity[payload.id]++
                state.totalItems++;
                state.totalAmount += payload.price
                state.installment = Math.max(state.installment, payload.installments)
            } else {
                state.quantity[payload.id] = 1
                state.totalItems++;
                state.cartItems.push(payload)
                state.installment = Math.max(state.installment, payload.installments)
                state.totalAmount += payload.price
            }
        },
        increase: (state, { payload }) => {
            state.quantity[payload.id]++
            state.totalItems++;
            state.totalAmount += payload.price
        },
        decrease: (state, { payload }) => {
            state.quantity[payload.id]--
            state.totalItems--;
            state.totalAmount -= payload.price
        },
        hide: (state) => {
            state.display = "hide"
        },
        deleted: (state, { payload }) => {
            const value = state.quantity[payload.id]
            state.totalAmount -= payload.price * value
            delete state.quantity[payload.id]
            state.totalItems -= value
            state.cartItems = state.cartItems.filter((item => item.id !== payload.id))
        },
        getSize: (state, { payload }) => {
            if (state.selectedSize.includes(payload[0])) {
                const index = state.selectedSize.indexOf(payload[0]);
                state.selectedSize.splice(index, 1)
            } else {
                state.selectedSize.push(payload[0])
            }
            if (state.selectedSize.length > 0) {
                const updatedData = {}
                state.selectedSize.map((size) => {
                    const sizeItem = (data.products).filter(item => (item.availableSizes).includes(size))
                    for (let product of sizeItem) {
                        updatedData[product.id] = product
                    }
                })
                state.products = Object.values(updatedData)
                state.length = state.products.length
            } else {
                state.products = data.products
                state.length = state.products.length
            }
        }
    }
})

export const { getSize, deleted, show, hide, increase, decrease } = productSlice.actions
export default productSlice.reducer