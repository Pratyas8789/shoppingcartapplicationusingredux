import React from 'react'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { } from '@reduxjs/toolkit'
import { useDispatch, useSelector } from 'react-redux'
import DisplayCart from './DisplayCart';
import { show } from '../Redux/reducer';


export default function DisplayProduct() {
    const { products } = useSelector((store) => store.allProduct)
    const dispatch = useDispatch()
    const allCards = products.map((product) => {
        return (
            <Card key={product.id} className='d-flex felx-column align-items-center w-25 m-4'>
                <Card.Img variant="top" className='h-75 mb-5' src={`./products/${product.id}.jpg`} />
                {product.title}
                {product.isFreeShipping &&
                    <div className='d-flex justify-content-end w-100 position-absolute'>
                        <div className='d-flex justify-content-center bg-black text-light'>
                            <p>Free shipping</p>
                        </div>
                    </div>
                }
                <Card.Text className='' >
                    <h3 className=" mt-4 d-flex justify-content-center" >{product.currencyFormat} {product.price}</h3>
                    {product.installments !== 0 ?
                        <h5 className="d-flex justify-content-center text-black-50">or {product.installments} x {product.currencyFormat} {(product.price / product.installments).toFixed(2)} </h5> : ""}
                </Card.Text>
                <Button className='w-100' variant="dark" onClick={() => dispatch(show(product))} >Add to cart</Button>
                <DisplayCart />
            </Card>
        );
    })

    return (
        <>
            {allCards}
        </>
    );
}
