import React from 'react'
import Card from 'react-bootstrap/Card';
import { useDispatch, useSelector } from 'react-redux';
import Button from 'react-bootstrap/Button';
import data from '../Components/Data/shopping-cart/data.json'
import { decrease, deleted, increase } from '../Redux/reducer';

export default function DisplayCartItems() {
    const { cartItems, quantity } = useSelector(store => store.allProduct)

    const dispatch = useDispatch()

    let displayCartItems;
    if (cartItems.length > 0) {
        displayCartItems = cartItems.map((item) => {
            return (
                <Card className='d-flex w-100 bg-black text text-light' >
                    <div className='d-flex justify-content-end'
                    >
                        <Button className='bg-black text-light border-0' onClick={() => dispatch(deleted(item))} variant="light">X</Button>
                    </div>
                    <Card.Body className='d-flex'
                    >
                        <Card.Img className='w-25 h-100' variant="top" src={`./products/${item.id}.jpg`} />

                        <Card.Text className='w-50 h-100 ps-3'  >
                            <p >{item.title}</p>
                            <p>{item.availableSizes[0]} | {item.style}</p>
                            <p>Quantity:{quantity[item.id]} </p>
                        </Card.Text>
                        <Card.Text className=' d-flex flex-column justify-content-between align-items-center w-25 h-100' >
                            <p className='text-warning'>{item.currencyFormat} {(item.price * quantity[item.id]).toFixed(2)}</p>
                            <div className=' d-flex justify-content-center'>
                                {quantity[item.id] > 1 ?
                                    <Button onClick={() => dispatch(decrease(item))} variant="dark">-</Button> : <Button disabled variant="dark">-</Button>}
                                <Button onClick={() => dispatch(increase(item))} variant="dark">+</Button>
                            </div>
                        </Card.Text>

                    </Card.Body>
                </Card>
            );
        })
    }
    return (
        <div  >
            {displayCartItems}
        </div>
    )
}
