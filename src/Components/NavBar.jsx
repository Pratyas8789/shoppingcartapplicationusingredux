import React from 'react'
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import { } from '@reduxjs/toolkit'
import { useSelector } from 'react-redux'

export default function NavBar() {
    const { totalItems } = useSelector(store => store.allProduct)
    return (
        <Navbar className='d-flex justify-content-between p-0' bg="light" variant="dark" >
            <img
                alt=""
                src={`./products/logo.png`}
                width="60"
                height="60"
                className="d-inline-block align-top"
            />{' '}
            <button className='d-flex flex-column border-0' data-bs-toggle="offcanvas" data-bs-target="#offcanvasScrolling">
                <img
                    alt=""
                    src={`./products/bag-icon.jpg`}
                    width="50"
                    height="50"
                    className="d-inline-block align-top bg-black "
                />{' '}
                <Button className='ms-3 rounded-circle'
                    variant="warning"  >{totalItems}</Button>{' '}
            </button>
        </Navbar>
    );
}