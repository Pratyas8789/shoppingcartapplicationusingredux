import React from 'react'
import { } from '@reduxjs/toolkit'
import { useSelector, useDispatch } from 'react-redux'
import { hide } from '../Redux/reducer'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import DisplayCartItems from './DisplayCartItems';

export default function DisplayCart() {
    const { display, totalItems, installment, totalAmount } = useSelector(store => store.allProduct)
    const dispatch = useDispatch()
    return (
        <>
            <div class={`w-25 displayCart bg-black offcanvas offcanvas-end ${display}`} data-bs-scroll="true" data-bs-backdrop="false" tabindex="-1" id="offcanvasScrolling" aria-labelledby="offcanvasScrollingLabel">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasScrollingLabel"></h5>
                    <button type="button" onClick={() => dispatch(hide())} class="btn-close bg-secondary" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div className='w-100 d-flex  flex-column align-items-center' >
                    <div className='w-100 d-flex  justify-content-center' >
                        <div className='w-25 d-flex flex-column align-items-center' >
                            <img
                                alt=""
                                src={`./products/bag-icon.jpg`}
                                width="50"
                                height="50"
                            />{' '}
                            <Button className='ms-3 rounded-circle'
                                variant="warning">{totalItems}</Button>{' '}
                        </div>
                        <div className='w-25'>
                            <h3 className='text-white mt-3' >CART</h3>
                        </div>
                    </div>

                </div>
                <div class="offcanvas-body"   >
                    <DisplayCartItems />
                </div>
                <div className='w-100 bg-secondary'  >
                    <Card className='text-light bg-dark'>
                        <Card.Body >
                            <Card.Text className='d-flex justify-content-between'  >
                                <h4 >SUBTOTAL</h4>
                                <div className='d-flex flex-column align-items-end'   >
                                    <h4 className='text-warning'>${totalAmount.toFixed(2)}</h4>
                                    <p>OR UP TO {installment}x {(totalAmount / installment).toFixed(2)} </p>
                                </div>
                            </Card.Text>
                            <Button className='w-100 bg-black' onClick={() => {
                                alert(`Checkout - Subtotal: $ ${totalAmount.toFixed(2)}`)
                            }} variant="dark">CECKOUT</Button>
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </>
    )
}
