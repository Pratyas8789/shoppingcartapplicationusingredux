import React, { useContext } from 'react'
import Card from 'react-bootstrap/Card';
import { useDispatch, useSelector } from 'react-redux';
import { } from 'react-redux'
import { getSize } from '../Redux/reducer';

export default function AvailableSizes() {
    const { allSizes, selectedSize } = useSelector(store => store.allProduct)
    const dispatch = useDispatch()

    const allSize = (Object.entries(allSizes)).map((size) => {
        return (
            <button key={size} id={`button-${size[0]}`} onClick={() => dispatch(getSize(size))} className={` allSizeButton border-0 m-2 rounded-circle ${selectedSize.includes(size[0]) ? "bg-dark" : "bg-light"} ${selectedSize.includes(size[0]) ? "text-light" : "text-black"}`}>
                {size[0]}
            </button>
        )
    })
    return (
        <Card className='m-5 border-0'>
            <Card.Body>
                <Card.Title>Sizes:</Card.Title>
                <Card.Text className='d-flex flex-wrap' >
                    {allSize}
                </Card.Text>
            </Card.Body>
        </Card>
    )
}